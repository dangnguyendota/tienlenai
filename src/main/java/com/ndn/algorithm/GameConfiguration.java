package com.ndn.algorithm;

import com.ndn.base.BaseObject;

public class GameConfiguration {
    public boolean[] passed = new boolean[4];
    public int maxPlayer = 4;
    public int previousPlayer = 0;
    public BaseObject lastDealt = null;
    public int currentPlayer = 0;
    public boolean gang_beat = false;
    public boolean first_turn = false;
    public boolean using_heuristic = true;

}
